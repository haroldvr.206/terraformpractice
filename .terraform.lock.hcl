# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/community-terraform-providers/ignition" {
  version     = "2.1.2"
  constraints = "2.1.2"
  hashes = [
    "h1:Br5jfFMm/Ks09k6XKiux5kiUOZlIoOKewvH00dwXdL4=",
    "zh:097a118ede40d9ba4824ffae2024e380455ad5252983f13942ccd6f33dc84e29",
    "zh:3b8f2c57950a8c8cb814da3a7c64b0ef350a3aac345ce2a87c86ecc80894a1f3",
    "zh:507f5317622a00961a2b7a5dc7a47eb8fa9c8d61ec65a60e5b84a6c1cee25fe3",
    "zh:85fccd324ba8655136343ca702aef179e696fce4bb4ea887255bbb109dc651e2",
    "zh:988218424ca391d2fbed89d8c13ece873260bc08cae201254aceda170738bcf3",
    "zh:996c5195caff0848a0dedf99e58ee1460fe9c9223ae95d837c30402ec55f43a8",
    "zh:9b34179f921dcadaf265795a8534bffc5546ee894a26c45f5c13246819bb0722",
    "zh:b8604e6fff5244314c154166037b7a637722c2919623731c2a09d7f874b89515",
    "zh:c76b068b89025f2a205e71b6025d1611d8d1f88d81af84106878a169f0f1c742",
    "zh:f19aafd5337515e01567979ac2f75e90baf092db92bd3a33e11d256f128ddd2a",
    "zh:f74c97517c380404a2abb2edaa38838a1d17a24cac978f817d5410fcea6085da",
  ]
}

provider "registry.terraform.io/digitalocean/digitalocean" {
  version     = "2.19.0"
  constraints = "~> 2.0"
  hashes = [
    "h1:zcWk5fT8bx5vXdJzprX0nE0vEI//VxgfDxZNWCJL+1s=",
    "zh:01cee85343dee2dfc01499e19ef4e56b0c9260eee0a47329231cf500c07b4386",
    "zh:099eeddf9baf9c282430231da501a8b96b3fb28507ce3b78e3a185cc9d4b3860",
    "zh:191e090e8553355d91842163737d71051aeb499c8ddb23d2e8aae9dab2f8a1a5",
    "zh:25356abb47769270730b0ddb0a3eb89aec637395cdcb77c309d23e55839e4461",
    "zh:28876afb75ba5367d20e508e05c7657f90922142ff80d8a81a4d68b3381adb86",
    "zh:404a304e37c3dec8017318b16ab701553e5242dc2460211346a9dd39242709a6",
    "zh:40f53111b01fc78fdc7a6ba47a80d51c9a45e77e5b7d7d5bcae3a0c6f58ffbdf",
    "zh:48f212068234df3dcfe5544c96b10403b15a190203742756d7d0573ee0857c17",
    "zh:5189fe4fffdbff5c280f6741f55b2de9cb2b8c653cda0b2339c28cd1e3bc7884",
    "zh:a7d5840ca789a03a285c67d2838af4d8687c99f3e8fac4ce56fcd23802a66156",
    "zh:c0bd3c4555e5d7e6c96d3add3ddd8e41aa0df9e4a4518ad3b7f1d726a4e0a9f4",
    "zh:d70a903a6d75533aa4713e255c9c967ec453195f2209439981f015f203805a6e",
    "zh:db8110736bd47f99213d72309ebb720718a80b15ddd46e34a8ee9b2125903079",
    "zh:e2180f334506601e0a6af8863159cc719ce584fdb23bd45ddc120f33d22cec19",
    "zh:eb515a24d231e7f1ef344b9b88fa2071f760ec34fbb47d80bbacdf7e35f3daca",
  ]
}
