// estructura, todos los recursos, las subredes, instancias, etc

resource "digitalocean_droplet" "do_droplet" {
  image  = "ubuntu-18-04-x64"
  name   = "UbuntuDropletForDevelop" 
  region = "nyc3"
  size   = "s-1vcpu-1gb"
  monitoring = true
  ssh_keys = [var.do_sshKeyFingerPrint]
  user_data = file("user-data.yml")
}
resource "digitalocean_project" "develop" {
  name        = "develop"
  description = "A project to represent development resources."
  purpose     = "Web Application"
  environment = "Development"
  resources = [digitalocean_droplet.do_droplet.urn]
}
