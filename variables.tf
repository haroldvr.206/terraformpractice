variable "env" {
  type = string
}
variable "owner" {
  type = string
}
variable "product" {
  type = string
}
variable "location" {
  type = string
}
variable "do_token" {
  type = string
}
variable "do_sshKeyFingerPrint" {
  type = string
}
variable "runner_name" {
  type = string
}
variable "gitlab_address" {
  type = string
}
variable "registration_token" {
  type = string
}

variable "UserName" {
  type = string
}